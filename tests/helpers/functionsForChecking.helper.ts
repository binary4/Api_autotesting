import { expect } from 'chai';

export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
  expect(response.statusCode, `Status Code should be ${statusCode}`).to.be.equal(statusCode);
}

export function checkResponseBodyStatus(response, status: string) {
  expect(response.body.status, `Status should be ${status}`).to.be.equal(status);
}

export function checkResponseBodyMessage(response, message: string) {
  expect(response.body.message, `Message should be ${message}`).to.be.equal(message);
}

export function checkResponseTime(response, maxResponseTime: number = 3000) {
  expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`)
    .to.be.lessThan(maxResponseTime);
}

export function checkJSONSchema(response, schema) {
  expect(response.body).to.have.jsonSchema(schema);
}

export function checkLengthGreater(response, length: number) {
  expect(response.body.length, `Number of items should be greater than ${length}`).to.be.greaterThan(length);
}

export function checkAuthorInfo(response, authorSettingsBody) {
  expect(
    response.body.firstName,
    `Author firstName should be ${authorSettingsBody.firstName}`
  ).to.be.equal(authorSettingsBody.firstName);
  expect(
    response.body.lastName,
    `Author lastName should be ${authorSettingsBody.lastName}`
  ).to.be.equal(authorSettingsBody.lastName);
  expect(
    response.body.biography,
    `Author biography should be ${authorSettingsBody.biography}`
  ).to.be.equal(authorSettingsBody.biography);
}

export function checkArticleInfo(response, articleBody) {
  expect(response.body.name, `Article name should be ${articleBody.name}`).to.be.equal(articleBody.name);
  expect(response.body.text, `Article text should be ${articleBody.text}`).to.be.equal(articleBody.text);
}

export function checkStudentInfo(response, studentSettingsBody) {
  expect(
    response.body.firstName
    , `Student firstName should be ${studentSettingsBody.firstName}`
  ).to.be.equal(studentSettingsBody.firstName);
  expect(
    response.body.job,
    `Student job should be ${studentSettingsBody.job}`
  ).to.be.equal(studentSettingsBody.job);
  expect(
    response.body.lastName,
    `Student lastName should be ${studentSettingsBody.lastName}`
  ).to.be.equal(studentSettingsBody.lastName);
}
