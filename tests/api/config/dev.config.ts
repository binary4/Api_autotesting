global.appConfig = {
  envName: 'DEV Environment',
  baseUrl: 'https://knewless.tk/api/',
  swaggerUrl: 'https://knewless.tk/api/swagger-ui/index.html',

  users: {
    author: {
      email: '666ih.qatester3@inbox.testmail.app',
      password: 'qatester123',
    },
    student: {
      email: '666ih.student2@inbox.testmail.app',
      password: 'student123',
    }
  },
};
