import { AuthController } from '../lib/controllers/auth.controller';
import {
  checkResponseBodyMessage,
  checkResponseBodyStatus,
  checkResponseTime,
  checkStatusCode
} from '../../helpers/functionsForChecking.helper';
const auth = new AuthController();

describe('Use test data', () => {
  let invalidCredentialsDataSet = [
    { email: '666ih.student2@inbox.testmail.app', password: '      ' },
    { email: '666ih.student2@inbox.testmail.app', password: 'student123 ' },
    { email: '666ih.student2@inbox.testmail.app', password: 'student 123' },
    { email: '666ih.student2@inbox.testmail.app', password: 'user' },
    { email: '666ih.student2@inbox.testmail.app', password: '666ih.student2@inbox.testmail.app' },
    { email: '666ih.student2 @inbox.testmail.app ', password: 'student123' },
    { email: '666ih.student2inbox.testmail.app  ', password: 'student123' },
  ];

  invalidCredentialsDataSet.forEach((credentials) => {
    it(`login using invalid credentials : ${credentials.email} + ${credentials.password}`, async () => {
      let response = await auth.authenticateUser(credentials);

      checkStatusCode(response, 401);
      checkResponseBodyStatus(response, 'UNAUTHORIZED');
      checkResponseBodyMessage(response, 'Bad credentials');
      checkResponseTime(response, 3000);
    });
  });
});
