import { expect } from 'chai';
import { AuthController } from '../lib/controllers/auth.controller';
import { UserController } from '../lib/controllers/user.controller';
import { StudentController } from '../lib/controllers/student.controller';
import { FavoriteController } from '../lib/controllers/favorite.controller';
import {
  checkJSONSchema, checkLengthGreater,
  checkResponseBodyMessage,
  checkResponseTime,
  checkStatusCode, checkStudentInfo
} from '../../helpers/functionsForChecking.helper';

const authController = new AuthController();
const studentController = new StudentController();
const userController = new UserController();
const favoriteController = new FavoriteController();
const schemas = require('./data/schemas_testData.json')
const chai = require('chai');
chai.use(require('chai-json-schema'));

let userId;
let studentId;
let courseId = '2f89aaec-62c2-46ac-9ab2-53b4fa0c3caf';
let isCourseFavorite;

const loginBody = global.appConfig.users.student;

const studentSettingsBody = {
  avatar: null,
  biography: "Hello World!",
  company: "Binary",
  direction: "IT Professional",
  education: "Master's degree",
  employment: "Employee",
  experience: 1,
  firstName: "Student",
  id: '',
  industry: "Software Products",
  job: "Quality Engineer",
  lastName: "Student",
  level: "Intermediate",
  location: "Ukraine",
  role: "QA",
  tags: [{
    id: "2f497d24-50d1-4d70-8c2f-1658a5524d79",
    imageSrc: "https://i.imgur.com/Em9yq6C.png",
    name: "SQL",
  }],
  uploadImage: null,
  website: "https://binary-studio.com",
  year: 1996,
}

describe('Student tests', () => {
  before('should get access token and user id', async () => {
    let response = await authController.authenticateUser(loginBody);

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkJSONSchema(response, schemas.schema_login);

    userController.tokenValue = response.body.accessToken;
    studentController.tokenValue = response.body.accessToken;
    favoriteController.tokenValue = response.body.accessToken;

    response = await studentController.getSettings();

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkJSONSchema(response, schemas.schema_studentSettings);

    userId = response.body.userId;
    studentId = response.body.id;
    studentSettingsBody.id = studentId;
  });

  it('set student settings', async () => {
    let response = await studentController.setSettings(studentSettingsBody);

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkResponseBodyMessage(response, 'Success. Your profile has been updated.')
  });

  it('get student info', async () => {
    let response = await studentController.getStudentInfo();

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkJSONSchema(response, schemas.schema_studentInfo);
    expect(response.body.id).to.be.equal(studentId);
    checkStudentInfo(response, studentSettingsBody);
  });

  it('get user', async () => {
    let response = await userController.getCurrentUser();

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkJSONSchema(response, schemas.schema_currentUser);
    expect(response.body.email, 'Response email should be the same as login email').to.be.equal(loginBody.email);
    expect(response.body.id, 'Response id should be the same as user id').to.be.equal(userId);
  });
});

describe('Test favorite', async () => {
  before('should  get access token and favorite course', async () => {
    let response = await authController.authenticateUser(loginBody);

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkJSONSchema(response, schemas.schema_login);

    favoriteController.tokenValue = response.body.accessToken;

    response = await favoriteController.getFavorite(courseId, 'COURSE');

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);

    isCourseFavorite = response.body;
  })

  it('change favorite', async () => {
    let response = await favoriteController.changeFavorite(courseId, 'COURSE');

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
  });

  it('get favorite courses', async () => {
    let response = await favoriteController.getFavoriteCourses();

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);

    if (isCourseFavorite) {
      expect(response.body.length).to.be.equal(0);
    } else {
      checkLengthGreater(response, 0);
      expect(response.body[response.body.length - 1].id).to.be.equal(courseId);
    }
  });
})
