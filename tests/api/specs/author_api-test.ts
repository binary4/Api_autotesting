import { expect } from 'chai';
import { AuthController } from '../lib/controllers/auth.controller';
import { AuthorController } from '../lib/controllers/author.controller';
import { UserController } from '../lib/controllers/user.controller';
import { ArticlesController } from '../lib/controllers/articles.controller';
import {
  checkArticleInfo,
  checkAuthorInfo,
  checkJSONSchema, checkLengthGreater,
  checkResponseBodyMessage,
  checkResponseTime,
  checkStatusCode
} from '../../helpers/functionsForChecking.helper';

const authController = new AuthController();
const authorController = new AuthorController();
const userController = new UserController();
const articlesController = new ArticlesController();
const schemas = require('./data/schemas_testData.json')
const chai = require('chai');
chai.use(require('chai-json-schema'));

let userId;
let authorId;
let articleId;
let commentId;

const loginBody = global.appConfig.users.author;

const authorSettingsBody = {
  avatar: "https://knewless.tk/assets/images/f1c2e603-b631-4c1e-b1c4-ab0224f19871.jpg",
  biography: "Hello world!!",
  company: "Binary",
  firstName: "Qa",
  id: '',
  job: "Quality Engineer",
  lastName: "Tester",
  location: "Ukraine",
  twitter: "https://twitter.com/",
  userId: '',
  website: "https://binary-studio.com",
}

const articleBody = {
  image: 'https://knewless.tk/assets/images/dcc27a23-d564-4b9f-a826-940ec6e1fef8.jpg',
  name: 'New article',
  text: '<p><strong>Hello world</strong></p>\n<p></p>\n',
};

describe('Author tests', () => {
  it('should get access token and user id', async () => {
    let response = await authController.authenticateUser(loginBody);

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkJSONSchema(response, schemas.schema_login);

    authorController.tokenValue = response.body.accessToken;
    userController.tokenValue = response.body.accessToken;
    articlesController.tokenValue = response.body.accessToken;

    response = await authorController.getSettings();

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkJSONSchema(response, schemas.schema_authorSettings);

    userId = response.body.userId;
    authorId = response.body.id;
    authorSettingsBody.id = authorId;
    authorSettingsBody.userId = userId;
  });

  it('author set', async () => {
    let response = await authorController.setSettings(authorSettingsBody);

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkResponseBodyMessage(response, 'Success. Your profile has been updated.')
  });

  it('get user', async () => {
    let response = await userController.getCurrentUser();

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkJSONSchema(response, schemas.schema_currentUser);
    expect(response.body.email, `Response email should be ${loginBody.email}`).to.be.equal(loginBody.email);
    expect(response.body.id, `Response id should be ${userId}`).to.be.equal(userId);
  });

  it('get public author', async () => {
    let response = await authorController.getPublicAuthor(authorId);

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkJSONSchema(response, schemas.schema_authorOverview);
    checkAuthorInfo(response, authorSettingsBody);
  });


  it('save article', async () => {
    let response = await articlesController.saveArticle(articleBody);

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);

    articleId = response.body.id;
  });

  it('get author articles', async () => {
    let response = await articlesController.getArticles();

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkLengthGreater(response, 0);
    checkJSONSchema(response, schemas.schema_authorArticles);
  });

  it('get article by Id', async () => {
    let response = await articlesController.getArticleById(articleId);

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkJSONSchema(response, schemas.schema_articleById);
    checkArticleInfo(response, articleBody);
  });

  it('save comment', async () => {
    let response = await articlesController.saveComment({
      articleId,
      text: "Hello World",
    });

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);

    commentId = response.body.id;
  });

  it('get comments by article', async () => {
    let response = await articlesController.getCommentsByArticle(articleId);

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkLengthGreater(response, 0);
    checkJSONSchema(response, schemas.schema_commentsByArticle);
  });
})
