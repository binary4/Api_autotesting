import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class StudentController {
  tokenValue;

  async getSettings() {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`student/`)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }

  async setSettings(body) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`student`)
      .body(body)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }

  async getStudentInfo() {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`student/info`)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }
}
