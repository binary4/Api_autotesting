import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class ArticlesController {
  tokenValue;

  async getArticles() {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`article/author`)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }

  async saveArticle(body) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`article`)
      .bearerToken(this.tokenValue)
      .body(body)
      .send();
    return response;
  }

  async getArticleById(articleId) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`article/${articleId}`)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }

  async saveComment(body) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`article_comment`)
      .bearerToken(this.tokenValue)
      .body(body)
      .send();
    return response;
  }

  async getCommentsByArticle(articleId) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`article_comment/of/${articleId}?size=200`)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }
}
