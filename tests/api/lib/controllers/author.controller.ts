import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class AuthorController {
  tokenValue;

  async getSettings() {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`author`)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }

  async setSettings(body) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`author`)
      .body(body)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }

  async getPublicAuthor(authorId) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`author/overview/${authorId}`)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }
}
