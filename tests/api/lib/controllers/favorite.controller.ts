import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class FavoriteController {
  tokenValue;

  async changeFavorite(id, type) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`favorite/change?id=${id}&type=${type}`)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }

  async getFavorite(id, type) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`favorite/?id=${id}&type=${type}`)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }

  async getFavoriteCourses() {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`favorite/courses`)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }
}
