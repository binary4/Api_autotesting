import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class AuthController {
  async authenticateUser(body) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`auth/login`)
      .body(body)
      .send();
    return response
  }
}
