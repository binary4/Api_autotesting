import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class UserController {
  tokenValue;

  async getCurrentUser() {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .bearerToken(this.tokenValue)
      .url(`user/me`)
      .send();
    return response
  }
}
